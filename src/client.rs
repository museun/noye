use std::cell::RefCell;
use std::io::{self, BufRead, BufReader, Write};
use std::net::TcpStream;
use std::process;
use std::str;
use std::thread;

use action::Action;

pub struct Client(RefCell<TcpStream>);

impl Client {
    pub fn new(addr: &str, port: u16) -> Self {
        match TcpStream::connect((addr, port)) {
            Ok(conn) => {
                info!("connected to {}:{}", addr, port);
                let conn = RefCell::new(conn);
                Client { 0: conn }
            }
            Err(err) => {
                error!("cannot connect to {}:{}: '{}'", addr, port, err);
                process::exit(2)
            }
        }
    }

    pub fn start<F: Fn(String) + 'static + Send>(&self, recv: F) {
        let stream = self.0
            .borrow()
            .try_clone()
            .expect("must be able to clone the stream");

        thread::spawn(move || Client::read_loop(&stream, recv));
    }

    pub fn register(&self, info: &(&str, &str, &str)) -> io::Result<()> {
        self.send(&format!("NICK {}", info.0))
            .and_then(|()| self.send(&format!("USER {} * 8 :{}", info.1, info.2)))
    }

    fn read_loop<F: Fn(String) + 'static + Send>(stream: &TcpStream, recv: F) {
        let reader = BufReader::new(stream).lines();
        for line in reader {
            match line {
                Ok(data) => {
                    trace!("< {}", data);
                    recv(data);
                }
                Err(err) => {
                    error!("cannot read data: '{}'", err);
                    process::exit(3)
                }
            }
        }
    }
}

pub trait Proto {
    fn join(&self, channel: &str) -> io::Result<()> {
        self.send(&format!("JOIN {}", channel))
    }

    fn part(&self, channel: &str, reason: Option<&str>) -> io::Result<()> {
        let data = match reason {
            Some(reason) => format!("PART {} :{}", channel, reason),
            None => format!("PART {}", channel),
        };
        self.send(&data)
    }

    fn quit(&self, reason: Option<&str>) -> io::Result<()> {
        let data = match reason {
            Some(reason) => format!("QUIT :{}", reason),
            None => "QUIT".into(),
        };
        self.send(&data)
    }

    fn privmsg(&self, target: &str, data: &str) -> io::Result<()> {
        self.send(&format!("PRIVMSG {} :{}", target, data))
    }

    fn notice(&self, target: &str, data: &str) -> io::Result<()> {
        self.send(&format!("NOTICE {} :{}", target, data))
    }

    fn action(&self, target: &str, data: &str) -> io::Result<()> {
        self.send(&format!("PRIVMSG {} :\u{1}ACTION {}\u{1}", target, data))
    }

    fn send(&self, raw: &str) -> io::Result<()>;

    fn test(&self, _action: Action) {}
}

impl Proto for Client {
    fn send(&self, raw: &str) -> io::Result<()> {
        for line in split(raw) {
            trace!("> {}", &line[..line.len() - 2]);
            let mut stream = self.0.borrow_mut();
            if let Err(err) = stream.write_all(line.as_bytes()) {
                return Err(err);
            }
        }

        let mut stream = self.0.borrow_mut();
        stream.flush()
    }
}

fn split(raw: &str) -> Vec<String> {
    if raw.len() > 510 && raw.contains(':') {
        let split = raw.splitn(2, ':').map(|s| s.trim()).collect::<Vec<&str>>();
        let (head, tail) = (split[0], split[1]);

        let mut vec = vec![];
        for part in tail.as_bytes()
            .chunks(510 - head.len() - 2)
            .map(str::from_utf8)
        {
            match part {
                Ok(part) => vec.push(format!("{} :{}\r\n", head, part)),
                Err(err) => {
                    warn!("dropping a split part: {}", err);
                    continue;
                }
            }
        }
        vec
    } else {
        vec![format!("{}\r\n", raw)]
    }
}
