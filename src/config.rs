#![macro_use]

extern crate toml;
use std::fs::File;
use std::io::{Error, ErrorKind, Read, Write};

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub server: ServerConfig,
    pub user: UserConfig,
    pub module: ModuleConfig,
    pub restart: RestartConfig,
}

#[derive(Deserialize, Serialize)]
pub struct ServerConfig {
    pub address: String,
    pub port: u16,
    pub autojoin: Vec<String>,
    pub quakenet: Option<QuakenetConfig>,
}

#[derive(Deserialize, Serialize)]
pub struct RestartConfig {
    pub owners: Vec<String>,
    pub address: String,
    pub port: u16,
}

#[derive(Deserialize, Serialize)]
pub struct QuakenetConfig {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Serialize)]
pub struct UserConfig {
    pub nickname: String,
    pub username: String,
    pub realname: String,
}

#[derive(Deserialize, Serialize, Default)]
pub struct ModuleConfig {
    pub youtube: Option<YoutubeConfig>,
    pub sendanywhere: Option<SendAnywhereConfig>,
    pub imgur: Option<ImgurConfig>,
}

#[derive(Deserialize, Serialize)]
pub struct YoutubeConfig {
    pub api_key: String,
}

#[derive(Deserialize, Serialize)]
pub struct SendAnywhereConfig {
    pub api_key: String,
}

#[derive(Deserialize, Serialize)]
pub struct ImgurConfig {
    pub api_key: String,
}

pub fn load_config() -> Option<Config> {
    #[cfg(not(test))]
    match File::open("config.toml").and_then(Config::load) {
        Ok(config) => Some(config),
        Err(err) => {
            error!("cannot load config.toml: '{}'", err);
            None
        }
    }
    #[cfg(test)]
    {
        let mut config = Config::default();
        config.server.autojoin = vec!["#test".into(), "#foobar".into()];
        config.server.quakenet = Some(QuakenetConfig {
            username: "test".into(),
            password: "password".into(),
        });
        Some(config)
    }
}

impl Config {
    pub fn default() -> Self {
        Config {
            server: ServerConfig {
                address: "localhost".into(),
                port: 6667,
                autojoin: vec![],
                quakenet: None,
            },
            restart: RestartConfig {
                owners: vec![],
                address: "localhost".into(),
                port: 54145,
            },
            user: UserConfig {
                nickname: "noye".into(),
                username: "noye".into(),
                realname: "noye in rust!".into(),
            },
            module: ModuleConfig {
                ..Default::default()
            },
        }
    }

    pub fn save(&mut self) {
        let name = "config.toml";
        match File::create(name) {
            Ok(mut file) => if let Err(err) = self.write(&mut file) {
                warn!("cannot save config.toml: {}", err);
            },
            Err(err) => {
                warn!("cannot open file to save config.toml: {}", err);
            }
        }
    }

    pub fn load<R: Read + 'static>(mut r: R) -> Result<Self, Error> {
        let mut data = String::new();
        r.read_to_string(&mut data)?;
        match toml::from_str(&data) {
            Ok(conf) => Ok(conf),
            _ => Err(Error::new(
                ErrorKind::Other,
                "could not parse configuration",
            )),
        }
    }

    fn write<W: Write>(&mut self, w: &mut W) -> Result<(), Error> {
        let data = toml::to_string(&self).expect("expected to serialize config to toml");
        w.write_all(data.as_bytes())
    }
}

#[macro_use]
macro_rules! api_key_here {
    () => {
        #[allow(unused_imports)]
        use lazy_static;
        use std::{cell::RefCell, sync::Mutex};

        lazy_static! {
            static ref API_KEY: Mutex<RefCell<String>> = Mutex::new(RefCell::new(String::new()));
        }
    };
}

#[macro_use]
macro_rules! get_api_key {
    ($func:expr, $ev:expr) => {{
        use lazy_static;

        #[cfg(not(test))]
        let key = match config::load_config() {
            Some(conf) => $func(conf),
            _ => None,
        };

        #[cfg(test)]
        let key = option_env!($ev).map(|s| s.to_string());

        let have_key = key.is_some();
        if have_key {
            lazy_static::initialize(&API_KEY);
            let s = &*API_KEY.lock().unwrap();
            *s.borrow_mut() = key.unwrap().clone();
        }
        have_key
    }};
}

#[macro_use]
macro_rules! with_api_key {
    ($func:expr) => {{
        let key = &*API_KEY.lock().unwrap();
        let key = &*key.borrow();
        $func(key.to_string())
    }};
}
