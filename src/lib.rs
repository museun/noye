#[macro_use]
extern crate log;
extern crate regex;
extern crate reqwest;
extern crate threadpool;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate lazy_static;

// extern crate failure;
// extern crate failure_derive;

pub mod action;
pub mod bot;
pub mod client;
pub mod config;
pub mod http;
pub mod message;
pub mod util;

pub mod prelude;
pub mod testing;

pub mod modules;
