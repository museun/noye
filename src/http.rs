use reqwest;
use serde_json::{self, Value};
use std::error::Error;

pub fn get_json(
    url: &str,
    query: Option<&[(&str, &str)]>,
    headers: Option<&[(&str, &str)]>,
) -> Result<Value, Box<Error>> {
    let mut client = reqwest::Client::new().get(url);

    if let Some(query) = query {
        client.query(query);
    }

    if let Some(headers) = headers {
        let mut map = reqwest::header::Headers::new();
        for (k, v) in headers.iter() {
            map.set_raw(k.to_string(), v.to_string());
        }
        client.headers(map);
    }

    match client.send().and_then(|ref mut resp| resp.text()) {
        Ok(body) => match serde_json::from_str::<Value>(&body) {
            Ok(json) => Ok(json),
            Err(err) => {
                warn!("cannot parse json: {}", err);
                Err(Box::new(err))
            }
        },
        Err(err) => {
            warn!("cannot get url: '{}' '{:?}' '{:?}'", url, query, headers);
            Err(Box::new(err))
        }
    }
}
