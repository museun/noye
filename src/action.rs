#![macro_use]
use client::Proto;
use message::Message;
use std::sync::{Arc, Mutex};

pub type Response = Vec<Action>;

#[derive(PartialEq, Debug)]
pub enum Action {
    Reply(String),
    Message(String),
    Emote(String),
    Raw(String),
    Nothing,
}

// used to swap target/source if its a direct message or a channel message
fn correct_target(msg: &Message) -> &str {
    if let Message::Privmsg {
        ref source,
        ref target,
        ..
    } = msg
    {
        if target[0..1] == *"#" {
            target
        } else {
            source
        }
    } else {
        unreachable!()
    }
}

impl Action {
    pub fn encode<P: Proto>(&self, proto: &Arc<Mutex<P>>, msg: &Message) {
        let res = match *self {
            Action::Reply(ref data) => match *msg {
                Message::Privmsg { ref source, .. } => {
                    let target = correct_target(&msg);
                    let proto = proto.lock().unwrap();
                    proto.privmsg(target, &format!("{}: {}", source, data))
                }
                _ => Ok(()),
            },
            Action::Message(ref data) => match *msg {
                Message::Privmsg { .. } => {
                    let target = correct_target(&msg);
                    let proto = proto.lock().unwrap();
                    proto.privmsg(target, data)
                }
                _ => Ok(()),
            },
            Action::Emote(ref data) => match *msg {
                Message::Privmsg { .. } => {
                    let target = correct_target(&msg);
                    let proto = proto.lock().unwrap();
                    proto.action(target, data)
                }
                _ => Ok(()),
            },
            Action::Raw(ref data) => {
                let proto = proto.lock().unwrap();
                proto.send(data)
            }
            Action::Nothing => Ok(()),
        };

        if let Err(err) = res {
            warn!("ran into an error handling action ({:?}): {}", *self, err)
        }
    }
}

pub enum ConversionType {
    Message,
    Reply,
}

pub fn remap_iter<I>(iter: I, ty: &ConversionType) -> Response
where
    I: IntoIterator<Item = String>,
{
    let iter = iter.into_iter();

    match *ty {
        ConversionType::Message => iter.map(Action::Message).collect::<Response>(),
        ConversionType::Reply => iter.map(Action::Reply).collect::<Response>(),
    }
}

#[macro_export]
macro_rules! reply {
    ($($arg:tt)*) => {
        vec![Action::Reply(format!($($arg)*))]
    };
}

#[macro_export]
macro_rules! replies {
    ($($x:expr),*) => {
        {
            let mut temp = Vec::new();
            $(
                temp.push(Action::Reply($x.into()));
            )*
            temp
        }
    };
}

#[macro_export]
macro_rules! message {
    ($($arg:tt)*) => {
        vec![Action::Message(format!($($arg)*))]
    };
}

#[macro_export]
macro_rules! messages {
    ($($x:expr),*) => {
        {
            let mut temp = Vec::new();
            $(
                temp.push(Action::Message($x.into()));
            )*
            temp
        }
    };
}

#[macro_export]
macro_rules! emote {
    ($($arg:tt)*) => {
        vec![Action::Emote(format!($($arg)*))]
    };
}

#[macro_export]
macro_rules! emotes {
    ($($x:expr),*) => {
        {
            let mut temp = Vec::new();
            $(
                temp.push(Action::Emote($x.into()));
            )*
            temp
        }
    };
}

#[macro_export]
macro_rules! raw {
    ($($arg:tt)*) => {
        vec![Action::Raw(format!($($arg)*))]
    };
}

#[macro_export]
macro_rules! raws {
    ($($x:expr),*) => {
        {
            let mut temp = Vec::new();
            $(
                temp.push(Action::Raw($x.into()));
            )*
            temp
        }
    };
}

#[macro_export]
macro_rules! nothing {
    () => {
        vec![Action::Nothing]
    };
}

#[macro_export]
macro_rules! bail_with_nothing {
    ($e:expr) => {
        match $e {
            Some(s) => s,
            None => return nothing!(),
        }
    };
    ($e:expr) => {
        match $e {
            Ok(s) => s,
            Err(_) => return nothing!(),
        }
    };
    () => {
        return nothing!();
    };
}
