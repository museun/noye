extern crate noye;
use noye::bot::Bot;
use noye::client::Client;
use noye::config;
use noye::modules::*;

extern crate chrono;
extern crate ctrlc;
extern crate fern;
extern crate log;

#[allow(unused_imports)]
use log::*;
use std::process;
use std::sync::{Arc, Condvar, Mutex};

fn main() {
    init_logger();

    let conf = match config::load_config() {
        Some(conf) => conf,
        None => {
            eprintln!("cannot load config.toml");
            process::exit(1);
        }
    };

    let client = Arc::new(Mutex::new(Client::new(
        &conf.server.address,
        conf.server.port,
    )));

    let mut bot = Bot::new(&Arc::clone(&client));
    init_handlers(&mut bot);

    let info = (
        conf.user.nickname.as_str(),
        conf.user.username.as_str(),
        conf.user.realname.as_str(),
    );

    {
        let client = Arc::clone(&client);
        let client = client.lock().unwrap();
        if let Err(err) = client.register(&info) {
            error!(
                "could not register with: {}, {} '{}': {}",
                info.0, info.1, info.2, err
            );
            process::exit(3);
        }
    }

    {
        let client = Arc::clone(&client);
        let client = client.lock().unwrap();
        client.start(move |data| bot.dispatch(&data));
    }

    run_until()();
}

fn init_handlers(mut bot: &mut Bot) {
    // builtin handlers
    builtin::register(&mut bot);
    // youtube handlers
    youtube::register(&mut bot);
    // sendanywhere handlers
    sendanywhere::register(&mut bot);
    // vimeo handlers
    vimeo::register(&mut bot);
    // link size handlers
    linksize::register(&mut bot);
    // imgur handlers
    imgur::register(&mut bot);
    // gfycat handlers
    gfycat::register(&mut bot);
    // naver handlers
    naver::register(&mut bot);
    // daum handlers
    daum::register(&mut bot);
}

fn run_until() -> Box<Fn()> {
    let locker = Arc::new((Mutex::new(true), Condvar::new()));

    let cloned = Arc::clone(&locker);
    ctrlc::set_handler(move || {
        let &(ref lock, ref cv) = &*cloned;
        let mut running = lock.lock().unwrap();
        *running = false;
        cv.notify_one();
    }).expect("should be able to set the Ctrl+C handler");

    let cloned = Arc::clone(&locker);
    Box::new(move || {
        let &(ref lock, ref cv) = &*cloned;
        let mut running = lock.lock().unwrap();
        while *running {
            running = cv.wait(running).unwrap();
        }
    })
}

fn init_logger() {
    fern::Dispatch::new()
        .format(|out, msg, record| {
            out.finish(format_args!(
                "[{}|{}] {}: {}",
                chrono::Local::now().format("%F %T%.3f"),
                record.level(),
                record.target(),
                msg
            ))
        })
        .level(log::LevelFilter::Warn)
        .level_for("noye", log::LevelFilter::Info)
        .chain(std::io::stdout())
        .chain(fern::log_file("noye.log").expect("could not create the log file"))
        .apply()
        .expect("could not create logger");
}
