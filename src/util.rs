use std::fmt::Write;
use std::time::Duration;

use message::Envelope;

pub trait CommaSeparated {
    fn with_commas(&self) -> String;
}

impl CommaSeparated for u64 {
    fn with_commas(&self) -> String {
        fn commaize(n: u64, f: &mut String) {
            if n < 1000 {
                write!(f, "{}", n).unwrap();
                return;
            }
            commaize(n / 1000, f);
            write!(f, ",{:03}", n % 1000).unwrap();
        }

        let mut buf = String::new();
        commaize(*self, &mut buf);
        buf
    }
}

pub trait FileSize {
    fn as_file_size(&self) -> String;
}

impl FileSize for u64 {
    fn as_file_size(&self) -> String {
        const SIZES: [&str; 5] = ["B", "KB", "MB", "GB", "TB"];

        let mut order = 0;
        let mut size = *self as f64;
        while size >= 1024.0 && order + 1 < SIZES.len() {
            order += 1;
            size /= 1024.0
        }

        format!("{:.2} {}", size, SIZES[order])
    }
}

pub trait TimeStamp {
    fn as_timestamp(&self) -> String;
    fn as_readable_time(&self, Option<ReadableTime>) -> String;
}

impl TimeStamp for Duration {
    fn as_timestamp(&self) -> String {
        let time = self.as_secs();
        let hours = time / (60 * 60);
        let minutes = (time / 60) % 60;
        let seconds = time % 60;

        if hours > 0 {
            format!("{:02}:{:02}:{:02}", hours, minutes, seconds)
        } else {
            format!("{:02}:{:02}", minutes, seconds)
        }
    }

    fn as_readable_time(&self, prefs: Option<ReadableTime>) -> String {
        let mut secs = self.as_secs();
        if secs == 0 {
            return "0 seconds".into();
        }

        let opts = prefs.unwrap_or_default();
        let table = [
            ("year", 31_557_600, opts.year),
            ("month", 2_629_800, opts.month),
            // ("week", 604_800, opts.week), // this breaks it.
            ("day", 86_400, opts.day),
            ("hour", 3600, opts.hour),
            ("minute", 60, opts.minute),
            ("second", 1, opts.second),
        ];

        let mut list = vec![];
        for &(name, el, op) in &table {
            let div = secs / el;
            if div > 0 {
                if op {
                    let mut buf = format!("{} {}", div, name);
                    if div > 1 {
                        buf.push_str("s");
                    }
                    list.push(buf);
                }
                secs -= el * div;
            }
        }
        if list.len() > 1 {
            if list.len() > 2 {
                let len = list.len() - 2;
                for e in &mut list.iter_mut().take(len) {
                    e.push_str(",");
                }
            }
            let len = list.len() - 1;
            list.insert(len, "and".into())
        }
        list.join(" ")
    }
}

pub struct ReadableTime {
    pub year: bool,
    pub month: bool,
    pub week: bool,
    pub day: bool,
    pub hour: bool,
    pub minute: bool,
    pub second: bool,
}

impl Default for ReadableTime {
    fn default() -> Self {
        Self {
            year: false,
            month: false,
            week: false,
            day: true,
            hour: true,
            minute: true,
            second: true,
        }
    }
}

pub trait ISO8601 {
    fn from_iso8601(period: &str) -> Duration;
}

impl ISO8601 for Duration {
    fn from_iso8601(period: &str) -> Duration {
        let list = period.split(|c: char| !c.is_numeric()).collect::<Vec<_>>();
        let mut index = 0;
        let mut total = 0;
        for i in (0..list.len()).rev() {
            if !list[i].is_empty() {
                total += list[i].parse::<u64>().unwrap() * u64::pow(60, index);
                index += 1;
            }
        }
        Duration::from_secs(total)
    }
}

pub fn join_with<Input, Sep, I>(mut iter: I, sep: Sep) -> String
where
    Input: AsRef<str>,
    Sep: AsRef<str>,
    I: Iterator<Item = Input>,
{
    let mut buf = String::new();
    if let Some(s) = iter.next() {
        buf.push_str(s.as_ref());
    }
    for i in iter {
        buf.push_str(sep.as_ref());
        buf.push_str(i.as_ref());
    }
    buf
}

pub fn matches_for(s: &str, env: &Envelope) -> Option<Vec<String>> {
    match env.matches {
        Some(ref matches) => matches.get_many(s),
        None => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_humanize_duration() {
        let dur = Duration::from_secs(100 * 1024);
        let input = "1 day, 4 hours, 26 minutes and 40 seconds";
        assert_eq!(input, dur.as_readable_time(None));
    }

    #[test]
    fn test_iso8601_timestamp() {
        assert_eq!((5 * 60) + 5, Duration::from_iso8601("PT5M5S").as_secs());
        assert_eq!(
            (3 * 60 * 60) + (5 * 60) + 5,
            Duration::from_iso8601("PT3H5M5S").as_secs()
        );

        assert_eq!(5, Duration::from_iso8601("PT5S").as_secs());

        assert_eq!("05:05", Duration::from_iso8601("PT5M5S").as_timestamp());
        assert_eq!(
            "03:05:05",
            &Duration::from_iso8601("PT3H5M5S").as_timestamp()
        );
        assert_eq!("00:05", &Duration::from_iso8601("PT5S").as_timestamp());
    }

    #[test]
    fn test_file_sizes() {
        let input = [
            1234, 123456789, 12, 125, 999, 1001, 1999, 25713, 541236, 9874561,
        ];

        for input in &input {
            let _ = input.as_file_size();
        }
    }

    #[test]
    fn test_separate_commas() {
        let input = [
            ("1,234", 1234),
            ("123,456,789", 123456789),
            ("12", 12),
            ("125", 125),
            ("999", 999),
            ("1,001", 1001),
            ("1,999", 1999),
            ("25,713", 25713),
            ("541,236", 541236),
            ("9,874,561", 9874561),
        ];

        for &(expected, input) in &input {
            assert_eq!(expected, input.with_commas())
        }
    }
}
