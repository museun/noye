use action::{Action, Response};
use client::Proto;
use message::{Envelope, Matches, Message};

use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time;

use regex::Regex;
use threadpool::ThreadPool;

type ResponsePacket = (Arc<Message>, Response);

struct Inner {
    handlers: Vec<Arc<Mutex<Handler>>>,
    pool: ThreadPool,
    tx: mpsc::Sender<ResponsePacket>,
}

pub struct Bot {
    inner: Arc<Mutex<Inner>>,
    ping: Arc<Mutex<Option<u64>>>,
}

const PREFIX: char = '!';

const PING_TIMEOUT: u64 = 30;

impl Bot {
    pub fn new<P: Proto + 'static + Send>(proto: &Arc<Mutex<P>>) -> Self {
        type Sender = mpsc::Sender<ResponsePacket>;
        type Receiver = mpsc::Receiver<ResponsePacket>;
        let (tx, rx): (Sender, Receiver) = mpsc::channel();

        let reply_proto = Arc::clone(&proto);
        thread::spawn(move || {
            #[allow(unused_variables)]
            for (msg, resp) in rx.iter() {
                for action in resp.into_iter().filter(|a| *a != Action::Nothing) {
                    let proto = Arc::clone(&reply_proto);

                    // if we're not testing encode the message
                    #[cfg(not(test))]
                    action.encode(&proto, &msg);

                    // otherwise call the pseudo test handler on the protocol
                    #[cfg(test)]
                    {
                        let proto = proto.lock().unwrap();
                        proto.test(action);
                    }
                }
            }
        });

        let ping: Arc<Mutex<Option<u64>>> = Arc::new(Mutex::new(None));
        let ping_clone = Arc::clone(&ping);
        let ping_proto = Arc::clone(&proto);
        thread::spawn(move || {
            fn die(s: &str) {
                error!("{}", s);
                ::std::process::exit(4)
            }
            thread::sleep(::std::time::Duration::from_secs(PING_TIMEOUT));

            loop {
                let ping = ::std::time::SystemTime::now()
                    .duration_since(::std::time::UNIX_EPOCH)
                    .expect("cannot get system time")
                    .as_secs();

                {
                    let proto = ping_proto.lock().unwrap();
                    proto
                        .send(&format!("PING {}", ping))
                        .expect("should be able to send ping");
                }

                thread::sleep(::std::time::Duration::from_secs(PING_TIMEOUT));

                let last = ping_clone.lock().unwrap();
                match *last {
                    None => die("expected to get a ping response by now"),
                    Some(last) => {
                        if ping - last > PING_TIMEOUT {
                            die("possible ping timeout")
                        }
                    }
                }
            }
        });

        Self {
            inner: Arc::new(Mutex::new(Inner {
                handlers: vec![],
                pool: ThreadPool::new(8),
                tx,
            })),
            ping: Arc::clone(&ping),
        }
    }

    pub fn command(&mut self, cmd: &'static str, function: fn(&Envelope) -> Response) {
        self.add(Handler::Active(cmd, Box::new(function)))
    }

    pub fn passive(&mut self, re: &'static str, function: fn(&Envelope) -> Response) {
        let re = Arc::new(Regex::new(re).expect("must be able to compile the regex"));
        self.add(Handler::Passive(re, Box::new(function)))
    }

    pub fn event(&mut self, function: fn(&Message) -> Response) {
        self.add(Handler::Event(Box::new(function)))
    }

    fn add(&mut self, hn: Handler) {
        let mut inner = self.inner.lock().unwrap();
        inner.handlers.push(Arc::new(Mutex::new(hn)))
    }

    pub fn dispatch(&self, data: &str) {
        let msg = Arc::new(match Message::parse(data) {
            Ok(msg) => msg,
            Err(err) => {
                warn!("cannot parse '{}': {:?}", data, err);
                return;
            }
        });

        if let Message::Pong { ref data, .. } = *msg {
            if let Ok(data) = data.parse::<u64>() {
                let secs = time::Duration::from_secs(data).as_secs();
                let mut out = self.ping.lock().unwrap();
                *out = Some(secs);
            };
        }

        let inner = self.inner.lock().unwrap();
        for handler in &inner.handlers {
            Bot::execute(
                &inner.pool,
                &inner.tx,
                Arc::clone(&msg),
                Arc::clone(handler),
            )
        }
    }

    fn execute(
        pool: &ThreadPool,
        out: &mpsc::Sender<(Arc<Message>, Response)>,
        msg: Arc<Message>,
        handler: Arc<Mutex<Handler>>,
    ) {
        let out = out.clone();

        pool.execute(move || {
            let handler = &*handler.lock().unwrap();
            let mut resp = vec![];
            match handler {
                Handler::Active(cmd, func) => {
                    if let Message::Privmsg { ref data, .. } = *msg {
                        if let Some(param) = Self::match_command(cmd, data) {
                            resp.extend(func(&Envelope::new(&msg, param, None)))
                        }
                    }
                }
                Handler::Passive(re, func) => {
                    if let Message::Privmsg { ref data, .. } = *msg {
                        let re = Arc::clone(&re);
                        if re.is_match(data) {
                            let matches = Matches::create(data, &re);
                            resp.extend(func(&Envelope::new(&msg, None, Some(matches))))
                        }
                    }
                }
                Handler::Event(func) => resp.extend(func(&msg)),
            };

            if !resp.is_empty() {
                out.send((Arc::clone(&msg), resp))
                    .expect("send to output channel");
            }
        })
    }

    #[allow(unknown_lints)]
    #[allow(option_option)]
    fn match_command(cmd: &str, data: &str) -> Option<Option<String>> {
        let query = format!("{}{}", PREFIX, cmd);
        if !data.starts_with(&query) {
            return None;
        }
        if data == query {
            return Some(None);
        }
        let param = data.splitn(2, &query)
            .map(|s| s.trim())
            .find(|s| !s.is_empty())
            .map(|d| d.into());
        Some(param)
    }
}

pub enum Handler {
    Active(&'static str, Box<Fn(&Envelope) -> Response + Send>),
    Passive(Arc<Regex>, Box<Fn(&Envelope) -> Response + Send>),
    Event(Box<Fn(&Message) -> Response + Send>),
}
