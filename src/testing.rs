use action::{Action, Response};
use bot::Bot;
use client::Proto;

use std::cell::RefCell;
use std::io;
use std::sync::{mpsc, Arc, Mutex};
use std::time::Duration;

#[cfg(test)]
extern crate fern;

#[cfg(test)]
extern crate log;
#[allow(unused_imports)]
#[cfg(test)]
use log::*;

struct Inner {
    tx: mpsc::Sender<Action>,
}

struct TestProto {
    inner: Arc<Mutex<Inner>>,
}

impl TestProto {
    pub fn new() -> (Self, mpsc::Receiver<Action>) {
        let (tx, rx) = mpsc::channel();
        let inner = Arc::new(Mutex::new(Inner { tx }));
        (Self { inner }, rx)
    }
}

impl Proto for TestProto {
    fn send(&self, _: &str) -> io::Result<()> {
        Ok(())
    }

    fn test(&self, action: Action) {
        let inner = Arc::clone(&self.inner);
        let inner = inner.lock().unwrap();
        inner.tx.send(action).expect("should send action")
    }
}

pub struct Environment {
    bot: RefCell<Bot>,
    rx: mpsc::Receiver<Action>,
}

impl Environment {
    pub fn new_with_log() -> Self {
        #[cfg(test)]
        fern::Dispatch::new()
            .format(|out, msg, record| {
                out.finish(format_args!(
                    "[{}] {}: {}",
                    record.level(),
                    record.target(),
                    msg
                ))
            })
            .level(log::LevelFilter::Warn)
            .level_for("noye", log::LevelFilter::Trace)
            .chain(io::stdout())
            .apply()
            .expect("could not create logger");

        Environment::new()
    }
    pub fn new() -> Self {
        let (proto, rx) = TestProto::new();
        let bot = RefCell::new(Bot::new(&Arc::new(Mutex::new(proto))));
        Self { bot, rx }
    }

    pub fn with(&self, f: fn(&mut Bot)) {
        f(&mut self.bot.borrow_mut())
    }

    pub fn send_raw(&self, data: &str) {
        self.bot.borrow().dispatch(&data);
    }

    pub fn send(&self, data: &str) {
        let msg = format!(":test_user!user@localhost PRIVMSG #test_channel :{}", data);
        self.bot.borrow().dispatch(&msg)
    }

    pub fn read(&self) -> Response {
        vec![
            self.rx
                .recv_timeout(Duration::from_secs(10))
                .expect("should have gotten an action"),
        ]
    }

    pub fn read_by(&self, dur: Duration) -> Response {
        vec![
            self.rx
                .recv_timeout(dur)
                .expect("should have gotten an action"),
        ]
    }

    pub fn try_read_by(&self, dur: Duration) -> Result<Action, mpsc::RecvTimeoutError> {
        self.rx.recv_timeout(dur)
    }
}

impl Default for Environment {
    fn default() -> Self {
        Self::new()
    }
}
