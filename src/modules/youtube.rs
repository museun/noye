use prelude::*;
use serde_json::Value;

const VIDEO_REGEX: &str =
    r"(?:youtu\.be/|youtube.com/(?:\S*(?:v|video_id)=|v/|e/|embed/))(?P<id>[\w\-]{11})";

const CHANNEL_REGEX:&str =
    r"(?:youtu\.be/|youtube.com/(?:\S*(?:channel/(?P<channel>[\w\-]+))|(?:user/(?P<user>[\w\-]+))))";

static BASE_URI: &str = "https://www.googleapis.com/youtube/v3/";

api_key_here!{}

pub fn register(bot: &mut Bot) {
    let got_key = get_api_key! { |conf: config::Config| {
        conf.module.youtube.map(|m| m.api_key)
    }, "NOYE_TEST_YOUTUBE_API" };

    if !got_key {
        warn!("cannot load youtube handlers, api key is missing");
        return;
    }

    bot.passive(VIDEO_REGEX, hear_video);
    bot.passive(CHANNEL_REGEX, hear_channel);
}

fn hear_video(env: &Envelope) -> Response {
    let ids = bail_with_nothing!(env.matches_for("id"));
    lookup_videos(&ids).map_or(nothing!(), |json| {
        format_videos(&json).map_or(nothing!(), |list| {
            remap_iter(list, &ConversionType::Message)
        })
    })
}

fn lookup_videos<T: AsRef<str>>(ids: &[T]) -> Option<Value> {
    static PART: &str = "statistics,snippet,liveStreamingDetails,contentDetails";
    static FIELDS: &str = "items(id,statistics,liveStreamingDetails, \
                           snippet(title,channelTitle,channelId,liveBroadcastContent,publishedAt), \
                           contentDetails(duration))";

    let ids = join_with(ids.iter(), ",");
    let key = with_api_key!{|key| key};
    let query = &[
        ("id", ids.as_str()),
        ("part", PART),
        ("fields", FIELDS),
        ("key", &key),
    ];

    http::get_json(&format!("{}{}", BASE_URI, "videos"), Some(query), None).ok()
}

fn format_videos(val: &Value) -> Option<Vec<String>> {
    fn format_video(item: &Value) -> Option<String> {
        let ts = &item["contentDetails"]["duration"].as_str()?;
        let duration = ::std::time::Duration::from_iso8601(&ts).as_timestamp();
        let views = try_as::<u64>(&item["statistics"]["viewCount"])?.with_commas();
        let title = &item["snippet"]["title"].as_str()?;
        let channel = &item["snippet"]["channelTitle"].as_str()?;
        let id = &item["id"].as_str()?;
        Some(format!(
            "{} | {} · {} · {} | https://youtu.be/{} ",
            title, channel, duration, views, id
        ))
    }

    val.get("items").and_then(|item| {
        item.as_array().map(|array| {
            let iter = array.iter().filter_map(format_video);
            iter.collect::<Vec<_>>()
        })
    })
}

fn hear_channel(env: &Envelope) -> Response {
    let mut resp = vec![];
    if let Some(users) = env.matches_for("user") {
        for user in users {
            if let Some(json) = lookup_channel_by_name(&user) {
                if let Some(channel) = format_channels(&json) {
                    resp.extend(channel);
                }
            }
        }
    }

    if let Some(ids) = env.matches_for("channel") {
        if let Some(json) = lookup_channels(&ids) {
            if let Some(channels) = format_channels(&json) {
                resp.extend(channels);
            }
        }
    }

    remap_iter(resp, &ConversionType::Message)
}

fn lookup_channel_by_name(name: &str) -> Option<Value> {
    static FIELDS: &str = "items(etag,id,snippet(title,description,publishedAt),statistics,status)";

    let key = with_api_key!{|key| key};
    let query = &[
        ("forUsername", name),
        ("part", "snippet,statistics"),
        ("fields", FIELDS),
        ("key", &key),
    ];

    let url = &format!("{}{}", BASE_URI, "channels");
    http::get_json(url, Some(query), None).ok()
}

fn lookup_channels<T: AsRef<str>>(ids: &[T]) -> Option<Value> {
    static FIELDS: &str = "items(etag,id,snippet(title,description,publishedAt),statistics,status)";

    let ids = join_with(ids.iter(), ",");
    let key = with_api_key!{|key| key};
    let query = &[
        ("id", ids.as_str()),
        ("part", "snippet,statistics"),
        ("fields", FIELDS),
        ("key", &key),
    ];

    let url = &format!("{}{}", BASE_URI, "channels");
    http::get_json(url, Some(query), None).ok()
}

fn format_channels(val: &Value) -> Option<Vec<String>> {
    let items = val["items"].as_array()?.iter();
    let mut res = vec![];

    for item in items {
        let id = &item["id"].as_str()?;
        let title = &item["snippet"]["title"].as_str()?;
        let videos = try_as::<u64>(&item["statistics"]["videoCount"])?.with_commas();
        let views = try_as::<u64>(&item["statistics"]["viewCount"])?.with_commas();
        // let description = &item["snippet"]["description"].as_str()?;
        // let re = ::regex::Regex::new(r"(\s{2,}|\r\n|\n|\r)").expect("regex must compile");
        // let description = re.replace_all(description, " ");

        res.push(format!(
            "{} · {} videos. {} views | https://youtube.com/channel/{}",
            title, videos, views, id
        ));
    }

    Some(res)
}

fn try_as<T: ::std::str::FromStr>(val: &Value) -> Option<T> {
    val.as_str()?.parse::<T>().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_hear_video() {
        let env = Environment::new();
        env.with(register);

        let expected = "モーニング娘。'14 『見返り美人』(Morning Musume。\
            '14[A looking back beauty]) (Promotion Ver.) | モーニング娘。 ’18 · 05:05 ·";

        env.send("test https://www.youtube.com/watch?v=fFN4NCdtaXw");
        match env.read()[0] {
            Action::Message(ref s) => assert!(s.starts_with(expected)),
            _ => panic!("should've gotten a message"),
        };
    }
    #[test]
    fn test_hear_channel() {
        let env = Environment::new();
        env.with(register);

        env.send("https://www.youtube.com/user/Druaga1");
        env.send("https://www.youtube.com/channel/UCknFWPe4zoX9bs_lURjPenA");
        env.send("https://www.youtube.com/channel/UC-7I1gU1r6PxBSl87o-7YEQ");

        println!("{:?}", env.read());
        println!("{:?}", env.read());
        println!("{:?}", env.read());

        env.send(
            "https://www.youtube.com/user/Druaga1 \
             https://www.youtube.com/channel/UCknFWPe4zoX9bs_lURjPenA \
             https://www.youtube.com/channel/UC-7I1gU1r6PxBSl87o-7YEQ",
        );
        println!("{:?}", env.read());
        println!("{:?}", env.read());
        println!("{:?}", env.read());
    }
}
