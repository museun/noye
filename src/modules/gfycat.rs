use prelude::*;
use serde_json::Value;
use std::fmt;

pub fn register(bot: &mut Bot) {
    bot.passive(r"(?:gfycat\.com/(?P<id>.+?)\b)", hear_gfycat);
}

fn hear_gfycat(env: &Envelope) -> Response {
    let ids = bail_with_nothing!(env.matches_for("id"));
    let mut res = vec![];
    for id in ids {
        let url = format!("http://gfycat.com/cajax/get/{}", id);
        if let Ok(json) = http::get_json(&url, None, None) {
            if let Some(item) = GfycatItem::from_json(&json) {
                res.push(format!("{}", item));
            }
        }
    }

    remap_iter(res, &ConversionType::Message)
}

#[derive(Debug)]
struct GfycatItem<'a> {
    title: Option<&'a str>,
    description: Option<&'a str>,
    url: Option<&'a str>,
    views: u64,
    size: u64,
    width: u64,
    height: u64,
    nsfw: bool,
}

impl<'a> fmt::Display for GfycatItem<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.nsfw {
            write!(f, "[NSFW] ")?;
        }

        let title = if let Some(title) = self.title {
            write!(f, "{} ", title)?;
            if let Some(desc) = self.description {
                write!(f, "· {} ", desc)?;
            }
            true
        } else {
            false
        };

        if let Some(url) = self.url {
            if title {
                write!(f, "| ")?;
            }
            write!(f, "{} ", url)?;
        }

        if title {
            write!(f, "(")?;
        }
        write!(
            f,
            "{} views. {}x{}, {}",
            self.views.with_commas(),
            self.width,
            self.height,
            self.size.as_file_size()
        )?;

        if title {
            write!(f, ")")?;
        }

        Ok(())
    }
}

impl<'a> GfycatItem<'a> {
    pub fn from_json(json: &'a Value) -> Option<Self> {
        let item = &json["gfyItem"];

        let title = item["title"].as_str();
        let description = item["description"].as_str();
        let url = item["url"].as_str();

        let views = item["views"].as_u64()?;
        let size = item["webmSize"].as_str()?.parse::<u64>().ok()?;
        let width = item["width"].as_str()?.parse::<u64>().ok()?;
        let height = item["height"].as_str()?.parse::<u64>().ok()?;

        let nsfw = item["nsfw"].as_str()? == "1";

        Some(Self {
            title,
            description,
            url,
            size,
            views,
            width,
            height,
            nsfw,
        })
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     use testing::Environment;

//     #[test]
//     fn test_dev() {
//         let env = Environment::new();
//         env.with(register);
//         env.send("https://gfycat.com/AcclaimedTastyDamselfly");
//         env.send("https://gfycat.com/GrossShrillFish");
//         env.send("https://gfycat.com/ConcernedSimplisticGilamonster");

//         // TODO write actual tests here
//         env.read();
//         env.read();
//         env.read();
//     }
// }
