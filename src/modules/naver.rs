use prelude::*;

use regex::bytes::Regex;
use reqwest;
use std::io::Read;

pub fn register(bot: &mut Bot) {
    bot.passive(r"vlive.*/(?P<id>\d+)[/|\?]?", |env| {
        let ids = bail_with_nothing!(env.matches_for("id"));
        get_titles(&ids, &Service::Vlive)
    });

    bot.passive(r"tv\.naver\.com/v/(?P<id>\d+)/?", |env| {
        let ids = bail_with_nothing!(env.matches_for("id"));
        get_titles(&ids, &Service::Naver)
    });
}

fn get_titles<T: AsRef<str>>(ids: &[T], svc: &Service) -> Response {
    let mut res = vec![];
    for id in ids.iter() {
        let url = String::from(svc.base()) + id.as_ref();
        if let Some(title) = get_title(&url, &svc) {
            res.push(title);
        }
    }
    remap_iter(res, &ConversionType::Message)
}

enum Service {
    Vlive,
    Naver,
}

impl Service {
    fn base(&self) -> &'static str {
        match *self {
            Service::Vlive => "http://www.vlive.tv/video/",
            Service::Naver => "http://tv.naver.com/v/",
        }
    }
}

fn get_title(url: &str, svc: &Service) -> Option<String> {
    const VLIVE_RE: &str = r#"<meta property="og:title" content="(.*?)""#;
    const NAVER_RE: &str = r#"<meta property="og:title" content="(.*?)""#;

    let re = Regex::new(match svc {
        Service::Vlive => VLIVE_RE,
        Service::Naver => NAVER_RE,
    }).expect("must be able to compile title regex");

    let mut resp = reqwest::get(url).ok()?;
    let mut buf = [0; 4 * 1024]; // 4KB ought to be enough for anyone
    resp.read_exact(&mut buf).ok()?;

    let matches = re.captures(&buf)?.get(1)?;
    let buf = &buf[matches.start()..matches.end()];

    Some(::std::str::from_utf8(buf).ok()?.trim().into())
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_vlive() {
        let env = Environment::new();
        env.with(register);
        env.send("http://www.vlive.tv/video/67729");
        assert_eq!(
            message!("[V LIVE] 러블리즈(Lovelyz) “治癒(치유)” Album Preview"),
            env.read()
        );
    }

    #[test]
    fn test_tvcast() {
        let env = Environment::new();
        env.with(register);
        env.send("http://tv.naver.com/v/3064429");
        assert_eq!(
            message!("배드키즈 - 딱 하루 (Badkiz - Just One Day)"),
            env.read()
        );
    }
}
