use prelude::*;

const LINK_REGEX: &str = r"(sendanywhe\.re|send-anywhere\.com)/.*?(?P<id>[^/]*$)";

api_key_here!{}

pub fn register(bot: &mut Bot) {
    let got_key = get_api_key! { |conf: config::Config| {
        conf.module.sendanywhere.map(|m| m.api_key)
    }, "NOYE_TEST_SENDANYWHERE_API" };

    if !got_key {
        warn!("cannot load sendanywhere handlers, api key is missing");
        return;
    }

    bot.passive(LINK_REGEX, hear_link);
}

fn hear_link(env: &Envelope) -> Response {
    let ids = bail_with_nothing!(env.matches_for("id"));
    let key = with_api_key!(|key| key);

    let query = &[
        ("device_key", key.as_str()),
        ("mode", "list"),
        ("start_pos", "0"),
        ("end_pos", "30"),
    ];

    let mut res = vec![];
    for id in ids {
        let url = format!("https://send-anywhere.com/web/key/inquiry/{}", id);
        if let Ok(json) = http::get_json(&url, Some(query), None) {
            if let Some(list) = get_files(&id, query, &json) {
                res.extend(list);
            }
        }
    }

    remap_iter(res, &ConversionType::Message)
}

fn get_files(id: &str, query: &[(&str, &str)], val: &::serde_json::Value) -> Option<Vec<String>> {
    let server = &val["server"].as_str().unwrap();
    let expires = &val["expires_time"].as_u64().unwrap();
    let created = &val["created_time"].as_u64().unwrap();

    let url = format!("{}/webfile/{}", server, id);
    let val = http::get_json(&url, Some(query), None).ok().unwrap();
    let files = val["file"].as_array().unwrap();

    let format = |file: &::serde_json::Value| {
        let downloadable = &file["downloadable"].as_bool();
        if downloadable.is_none() || !downloadable.unwrap() {
            return None;
        }

        let name = &file["name"].as_str().unwrap();
        let size = &file["size"].as_u64().unwrap();
        let ts = ::std::time::Duration::from_secs(expires - created);

        Some(format!(
            "[{}] {} ({}, expires in ~{})",
            size.as_file_size(),
            name,
            id,
            ts.as_readable_time(None)
        ))
    };

    Some(files.iter().filter_map(format).collect::<Vec<_>>())
}

// can't test this because the files are temporary.
