use prelude::*;

pub fn register(bot: &mut Bot) {
    bot.passive(r"(?P<url>(?:www|https?)?reddit\.com[^\s]+)", hear_reddit);
}

fn hear_reddit(_env: &Envelope) -> Response {
    nothing!()
}
