use prelude::*;

pub fn register(bot: &mut Bot) {
    bot.passive(r"(?P<link>(?:www|https?)[^\s]+)", |env| {
        let links = bail_with_nothing!(env.matches_for("link"));
        message!("{}", bail_with_nothing!(lookup_size(links.into_iter())))
    });
}

fn lookup_size<I: Iterator<Item = String>>(iter: I) -> Option<String> {
    let mut sizes = vec![];
    for (i, link) in iter.enumerate() {
        if let Some(size) = get_size(&link) {
            if size > 10 * 1024 * 1024 {
                sizes.push((i + 1, size.as_file_size()))
            }
        }
    }

    if sizes.len() > 1 {
        Some(format!(
            "some of those are kind of big: {}",
            join_with(sizes.iter().map(|s| format!("#{}: {}", s.0, s.1)), ", ")
        ))
    } else if sizes.len() == 1 {
        Some(format!("that file is kind of big: {}", sizes[0].1))
    } else {
        None
    }
}

fn get_size(link: &str) -> Option<u64> {
    use reqwest;
    let client = reqwest::Client::new();
    let resp = client.head(link).send().ok()?;

    // try without HEAD if we're 401'd
    let resp = if resp.status() == reqwest::StatusCode::Unauthorized {
        client.get(link).send().ok()?
    } else {
        resp
    };

    Some(
        resp.headers()
            .get::<reqwest::header::ContentLength>()
            .map(|l| **l)
            .unwrap_or(0),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_redirect() {
        let env = Environment::new();
        env.with(register);
        env.send("http://attach.mail.daum.net/bigfile/v1/urls/d/ayhxrRcxJPe2WwVniCwxocna5ZA/lPgXiK0hDf7uhxinBhLSGQ");
        assert_eq!(
            message!("{}", "that file is kind of big: 3.01 GB"),
            env.read_by(::std::time::Duration::from_secs(5))
        );
    }

    #[test]
    fn test_single() {
        let env = Environment::new();
        env.with(register);
        env.send("http://speedtest-ams2.digitalocean.com/100mb.test");
        assert_eq!(
            message!("{}", "that file is kind of big: 100.00 MB"),
            env.read_by(::std::time::Duration::from_secs(5))
        );
    }

    #[test]
    fn test_multiple() {
        let env = Environment::new();
        env.with(register);
        env.send("http://speedtest-ams2.digitalocean.com/100mb.test http://speedtest-ams2.digitalocean.com/100mb.test");
        assert_eq!(
            message!(
                "{}",
                "some of those are kind of big: #1: 100.00 MB, #2: 100.00 MB"
            ),
            env.read_by(::std::time::Duration::from_secs(5))
        );
    }

    #[test]
    fn test_mixed() {
        let env = Environment::new();
        env.with(register);
        env.send("https://httpbin.org/bytes/1024 http://speedtest-ams2.digitalocean.com/100mb.test https://httpbin.org/bytes/1024 http://speedtest-ams2.digitalocean.com/100mb.test");
        assert_eq!(
            message!(
                "{}",
                "some of those are kind of big: #2: 100.00 MB, #4: 100.00 MB"
            ),
            env.read_by(::std::time::Duration::from_secs(5))
        );
    }
}
