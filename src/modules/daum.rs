use prelude::*;

pub fn register(bot: &mut Bot) {
    bot.passive(
        r"(?P<link>(:?[^\s]+)attach\.mail\.daum\.net/bigfile[^\s]+)",
        |env| {
            let links = bail_with_nothing!(env.matches_for("link"));
            let mut resp = vec![];
            for link in links {
                if let Some(title) = lookup_title(&link) {
                    resp.push(title);
                }
            }
            remap_iter(resp, &ConversionType::Message)
        },
    );
}

fn lookup_title(link: &str) -> Option<String> {
    use reqwest::{header::{ContentDisposition, DispositionParam},
                  Client};
    let client = Client::new();
    let resp = client.get(link).send().ok()?;

    let params = &resp.headers().get::<ContentDisposition>()?.parameters;
    match params.first() {
        Some(&DispositionParam::Filename(_, _, ref data)) => String::from_utf8(data.to_vec()).ok(),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_daum() {
        let env = Environment::new();
        env.with(register);
        env.send("http://attach.mail.daum.net/bigfile/v1/urls/d/ayhxrRcxJPe2WwVniCwxocna5ZA/lPgXiK0hDf7uhxinBhLSGQ");
        assert_eq!(
            message!(
                "170428 온스타일 겟잇뷰티 ALL NEW 2018 E14회 뷰티 꿀라보레이션.ts"
            ),
            env.read_by(::std::time::Duration::from_secs(5))
        );
    }
}
