use prelude::*;

pub fn register(bot: &mut Bot) {
    bot.passive(r"vimeo\.com/(?P<vid>\d+)", |env| {
        let vids = bail_with_nothing!(env.matches_for("vid"));
        let mut res = vec![];
        for vid in &vids {
            let url = format!("https://player.vimeo.com/video/{}/config", vid);
            // disable response compression because reqwest checks lens and vimeo lies
            let headers = &[("Accept-Encoding", "identity")];
            match http::get_json(&url, None, Some(headers)) {
                Ok(json) => {
                    if let Some(video) = parse_json(&json) {
                        res.push(video);
                    } else {
                        println!("cannot parse json");
                    }
                }
                Err(err) => {
                    println!("http request error: {:?}", err);
                }
            }
        }
        remap_iter(res, &ConversionType::Message)
    });
}

fn parse_json(json: &::serde_json::Value) -> Option<String> {
    let video = &json["video"];
    let duration = ::std::time::Duration::from_secs(video["duration"].as_u64()?);
    let id = video["id"].as_u64()?;
    let title = video["title"].as_str()?;
    let owner = video["owner"]["name"].as_str()?;

    Some(format!(
        "{} | {} · {} | https://vimeo.com/{}",
        title,
        duration.as_timestamp(),
        owner,
        id
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_vimeo() {
        let env = Environment::new();
        env.with(register);
        env.send("https://vimeo.com/264387948");
        assert_eq!(
            message!(
                "Acuvue - Teaser_쯔위편 | 00:12 · 617.grafik. | https://vimeo.com/264387948"
            ),
            env.read()
        );
    }
}
