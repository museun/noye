use prelude::*;

api_key_here!{}

pub fn register(bot: &mut Bot) {
    let got_key = get_api_key! { |conf: config::Config| {
        conf.module.imgur.map(|m| m.api_key)
    }, "NOYE_TEST_IMGUR_API" };

    if !got_key {
        warn!("cannot load imgur handlers, api key is missing");
        return;
    }

    bot.passive(r"imgur\.com/a/(?P<id>.*?(:?\s|$))", hear_imgur);
}

fn hear_imgur(env: &Envelope) -> Response {
    let ids = bail_with_nothing!(env.matches_for("id"));
    let key = with_api_key!{|key| key};

    let mut res = vec![];
    for id in ids {
        if let Some(album) = get_imgur_album(&id, &key) {
            res.push(album);
        }
    }

    remap_iter(res, &ConversionType::Message)
}

fn get_imgur_album(id: &str, key: &str) -> Option<String> {
    use std::fmt::Write;

    let url = format!("https://api.imgur.com/3/album/{}", id);
    let cid = format!("Client-ID {}", key);
    let headers: &[(&str, &str)] = &[("Authorization", &cid.as_str())];

    if let Ok(json) = http::get_json(&url, None, Some(headers)) {
        let data = &json["data"];
        let title = data["title"].as_str();
        let description = data["description"].as_str();

        let count = data["images_count"].as_u64()?;
        let views = data["views"].as_u64()?;
        let time = data["datetime"].as_u64()?;

        let nsfw = data["nsfw"].as_bool()?;
        let section = data["section"].as_str();

        let mut buf = String::new();

        if nsfw {
            write!(buf, "[NSFW] ").ok()?;
        }

        if let Some(title) = title {
            write!(buf, "{} |", title).ok()?;
        }

        if let Some(desc) = description {
            write!(buf, " {} ·", desc).ok()?;
        }

        let time = ::std::time::Duration::from_secs(time);
        write!(
            buf,
            "{} images · {} views. created {} ago.",
            count.with_commas(),
            views.with_commas(),
            time.as_readable_time(None)
        ).ok()?;

        if let Some(section) = section {
            write!(buf, "({})", section).ok()?;
        }

        Some(buf)
    } else {
        None
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     use testing::Environment;

//     #[test]
//     fn test_dev() {
//         let env = Environment::new();
//         env.with(register);
//         env.send("https://imgur.com/a/UQyo4");
//         env.read();
//     }
// }
