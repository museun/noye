use prelude::*;

use lazy_static;
use std::cell::RefCell;
use std::io::Write;
use std::net::TcpStream;
use std::sync::Mutex;
use std::time;

pub fn register(bot: &mut Bot) {
    lazy_static::initialize(&START);

    // auto respond to PINGs from server
    bot.event(|msg| match *msg {
        Message::Ping { ref data, .. } => raw!("PONG :{}", data),
        _ => nothing!(),
    });

    // join channels when invited
    bot.event(|msg| match *msg {
        Message::Invite { ref channel, .. } => raw!("JOIN {}", channel),
        _ => nothing!(),
    });

    // auth with Q and join channels on connect
    bot.event(|msg| match *msg {
        Message::Ready { ref nick } => config::load_config().map_or(nothing!(), |conf| {
            let mut resp = vec![];
            if let Some(qa) = conf.server.quakenet {
                resp.extend(raw!(
                    "PRIVMSG Q@CServe.quakenet.org :AUTH {} {}",
                    qa.username,
                    qa.password
                ));
                resp.extend(raw!("MODE {} +x", nick));
            }

            if !conf.server.autojoin.is_empty() {
                let channels = join_with(conf.server.autojoin.iter(), ",");
                resp.extend(raw!("JOIN {}", channels));
            }
            resp
        }),
        _ => nothing!(),
    });

    // 433 nick collision, try nick + "_";
    bot.event(|msg| match *msg {
        Message::NickCollision { ref nick } => raw!("NICK {}_", nick),
        _ => nothing!(),
    });

    // try to reclaim nick if our desired nick was seen quitting
    bot.event(|msg| match *msg {
        Message::Quit { ref nick } => {
            if let Some(conf) = config::load_config() {
                let me = &conf.user.nickname;
                if me == nick {
                    return raw!("NICK {}", me);
                }
            }
            nothing!()
        }
        _ => nothing!(),
    });

    // part current channel
    bot.command("part", |env| raw!("PART {}", env.target));

    // join specified channel
    bot.command("join", |env| {
        if let Some(param) = &env.param {
            raw!("JOIN {}", param)
        } else {
            nothing!()
        }
    });

    // send the current version
    bot.command("version", |_| {
        reply!(
            "version: {}{}",
            REPO,
            option_env!("NOYE_BUILD_GIT_HASH").unwrap()
        )
    });

    // send how long the bots been running for
    bot.command("uptime", |_| {
        let dur = {
            let start = &*START.lock().unwrap();
            let start = start.borrow();
            start.elapsed()
        };
        reply!("I've been running for: {}", dur.as_readable_time(None))
    });

    // set the bot respawn duration
    bot.command("respawn", |env| {
        if let Some((address, port)) = check_privilege(&env.sender) {
            let delay = match env.param {
                Some(ref param) => param,
                None => "15",
            };

            if let Ok(mut client) = TcpStream::connect((address.as_str(), port)) {
                if let Err(err) = client.write_fmt(format_args!("DELAY {}\0", delay)) {
                    return reply!("couldn't send RESTART command: '{}'", err);
                }
            }
        } else {
            return reply!("you cannot do that");
        }

        nothing!()
    });

    // have the bot restart
    bot.command("restart", |env| {
        if let Some((address, port)) = check_privilege(&env.sender) {
            if let Ok(mut client) = TcpStream::connect((address.as_str(), port)) {
                if let Err(err) = client.write_all(b"RESTART\0") {
                    return reply!("couldn't send RESTART command: '{}'", err);
                }
            }
        } else {
            return reply!("you cannot do that");
        }

        nothing!()
    });
}

fn check_privilege(nick: &str) -> Option<(String, u16)> {
    if let Some(conf) = config::load_config() {
        let restart = conf.restart;
        if restart.owners.iter().any(|x| x == nick) {
            return Some((restart.address, restart.port));
        }
    }

    return None;
}

const REPO: &str = "https://bitbucket.org/museun/noye/commits/";

lazy_static! {
    static ref START: Mutex<RefCell<time::Instant>> = Mutex::new(RefCell::new(time::Instant::now()));
}

#[cfg(test)]
mod tests {
    use super::*;
    use testing::Environment;

    #[test]
    fn test_ping() {
        let env = Environment::new();
        env.with(register);
        env.send_raw("PING :123456");

        let expected = raw!("PONG :123456");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_nick_collision() {
        let env = Environment::new();
        env.with(register);
        env.send_raw(":test.localhost 433 * noye :Nickname is already in use");

        let expected = raw!("NICK noye_");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_nick_reclaim() {
        let env = Environment::new();
        env.with(register);
        env.send_raw(":noye!noye@irc.localhost QUIT :Ping timeout");

        let expected = raw!("NICK noye");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_invite_autojoin() {
        let env = Environment::new();
        env.with(register);
        env.send_raw(":test_user!user@localhost INVITE test_bot :#test_channel");

        let expected = raw!("JOIN #test_channel");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_connect_autojoin() {
        let env = Environment::new();
        env.with(register);
        env.send_raw(":test.localhost 001 test :Welcome to IRC");

        env.read(); // ignore quakenet auth
        env.read(); // ignore mode +x

        let expected = raw!("JOIN #test,#foobar");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_connect_auth() {
        let env = Environment::new();
        env.with(register);
        env.send_raw(":test.localhost 001 test :Welcome to IRC");

        let expected = raw!("PRIVMSG Q@CServe.quakenet.org :AUTH test password");
        assert_eq!(expected, env.read());

        let expected = raw!("MODE test +x");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_join_command() {
        let env = Environment::new();
        env.with(register);
        env.send("!join #foobar");

        let expected = raw!("JOIN #foobar");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_part_command() {
        let env = Environment::new();
        env.with(register);
        env.send("!part");

        let expected = raw!("PART #test_channel");
        assert_eq!(expected, env.read());
    }

    #[test]
    fn test_version_command() {
        let env = Environment::new();
        env.with(register);
        env.send("!version");

        match env.read()[0] {
            Action::Reply(ref s) => assert!(s.starts_with("version:")),
            _ => panic!("expected to get version response"),
        }
    }

    #[test]
    fn test_uptime_command() {
        let env = Environment::new();
        env.with(register);
        env.send("!uptime");

        match env.read()[0] {
            Action::Reply(ref s) => assert!(s.starts_with("I've been running for:")),
            _ => panic!("expected to get uptime response"),
        }
    }
}
