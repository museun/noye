#![allow(unused_variables)]

use regex::Regex;
use std::collections::HashMap;

#[derive(Debug)]
pub enum ParseError {
    EmptySender,
    EmptyArgs,
    EmptyData,
    UnexpectedEnd,
}

#[derive(Clone, PartialEq, Debug)]
pub enum Message {
    Privmsg {
        source: String,
        target: String,
        data: String,
    },
    Ping {
        source: Option<String>,
        data: String,
    },
    Pong {
        source: Option<String>,
        data: String,
    },
    Ready {
        nick: String,
    },
    Invite {
        source: String,
        channel: String,
    },
    NickCollision {
        nick: String,
    },
    Quit {
        nick: String,
    },
    Unknown(String),
}

impl Message {
    pub fn parse(s: &str) -> Result<Message, ParseError> {
        let original = s.to_owned();

        let (s, source) = if s.starts_with(':') {
            let end = s.find(' ').ok_or(ParseError::UnexpectedEnd)?;
            let sender = {
                let s = &s[1..end];
                match s.find('!') {
                    Some(pos) => &s[..pos],
                    None => s,
                }
            };

            (&s[end + 1..], Some(sender))
        } else {
            (s, None)
        };

        let end = s.find(' ').ok_or(ParseError::UnexpectedEnd)?;
        let event = &s[..end];
        let (args, next) = match s.find(':') {
            Some(end2) => (&s[end + 1..end2], Some(end2)),
            None => (&s[end + 1..], None),
        };

        let mut args = args.split_whitespace();
        let data = next.map(|n| &s[n + 1..]);

        match event {
            "PRIVMSG" => {
                let source = source.map(|d| d.to_owned()).ok_or(ParseError::EmptySender)?;
                let target = args.next()
                    .map(|d| d.to_owned())
                    .ok_or(ParseError::EmptyArgs)?;
                let data = data.map(|d| d.to_owned()).ok_or(ParseError::EmptyData)?;

                Ok(Message::Privmsg {
                    source,
                    target,
                    data,
                })
            }
            "PING" => {
                let source = args.next().map(|d| d.to_owned());
                let data = data.map(|d| d.to_owned()).ok_or(ParseError::EmptyData)?;
                Ok(Message::Ping { source, data })
            }
            "PONG" => {
                let source = args.next().map(|d| d.to_owned());
                let data = data.map(|d| d.to_owned()).ok_or(ParseError::EmptyData)?;
                Ok(Message::Pong { source, data })
            }
            "INVITE" => {
                let source = args.next()
                    .map(|d| d.to_owned())
                    .ok_or(ParseError::EmptyArgs)?;
                let channel = data.map(|d| d.to_owned()).ok_or(ParseError::EmptyData)?;
                Ok(Message::Invite { source, channel })
            }
            "001" => {
                let nick = args.next()
                    .map(|d| d.to_owned())
                    .ok_or(ParseError::EmptyArgs)?;
                Ok(Message::Ready { nick })
            }
            "QUIT" => {
                //>> :museun!~museun@test.localhost QUIT :Quit: Leaving
                let nick = source.map(|d| d.to_owned()).ok_or(ParseError::EmptySender)?;
                Ok(Message::Quit { nick })
            }
            "433" => {
                let nick = args.nth(1)
                    .map(|d| d.to_owned())
                    .ok_or(ParseError::EmptyArgs)?;
                Ok(Message::NickCollision { nick })
            }
            _ => Ok(Message::Unknown(original)),
        }
    }
}

#[derive(Debug)]
pub struct Envelope {
    pub sender: String,
    pub target: String,
    pub param: Option<String>,
    pub matches: Option<Matches>,
}

impl Envelope {
    pub fn new(msg: &Message, param: Option<String>, matches: Option<Matches>) -> Self {
        let (sender, target) = {
            match msg {
                Message::Privmsg {
                    ref source,
                    ref target,
                    ..
                } => (source.to_owned(), target.to_owned()),
                _ => panic!("invalid message"),
            }
        };

        Self {
            sender,
            target,
            param,
            matches,
        }
    }

    pub fn matches_for(&self, s: &str) -> Option<Vec<String>> {
        match self.matches {
            Some(ref matches) => matches.get_many(s),
            None => None,
        }
    }
}

#[derive(Debug)]
pub struct Matches {
    map: HashMap<String, Vec<String>>,
}

impl Matches {
    pub fn create(input: &str, re: &Regex) -> Self {
        let mut map = HashMap::new();
        for captures in re.captures_iter(input) {
            for name in re.capture_names() {
                if let Some(name) = name {
                    let list = map.entry(name.into()).or_insert_with(|| vec![]);
                    if let Some(capture) = captures.name(name) {
                        list.push(capture.as_str().into());
                    }
                }
            }
        }
        Matches { map }
    }

    pub fn get(&self, key: &str) -> Option<String> {
        self.map.get(key).and_then(|s| s.get(0).cloned())
    }

    pub fn get_many(&self, key: &str) -> Option<Vec<String>> {
        self.map.get(key).cloned()
    }
}
