use std::process;

fn main() {
    if let Some((rev, branch)) = git_version() {
        println!("cargo:rustc-env=NOYE_BUILD_GIT_HASH={}", rev);
        println!("cargo:rustc-env=NOYE_BUILD_GIT_BRANCH={}", branch)
    }
}

fn git_version() -> Option<(String, String)> {
    fn git(args: &[&str]) -> Option<String> {
        let git = process::Command::new("git").args(args).output();
        git.ok().and_then(|out| {
            let v = String::from_utf8_lossy(&out.stdout).trim().to_string();
            if v.is_empty() {
                None
            } else {
                Some(v)
            }
        })
    }

    git(&["rev-parse", "--short=7", "HEAD"]).and_then(|revision| {
        git(&["rev-parse", "--abbrev-ref", "HEAD"]).and_then(|branch| Some((revision, branch)))
    })
}
